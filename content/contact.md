+++
title = "contact"
weight = 50
+++
You can use the form below to send us a message directly.

{{< contactform >}}

If you would rather use email: <a href='mailto&#58;in&#102;o%40%&#54;3ar%6&#52;%&#54;9%66f&#117;&#110;i%6&#51;ycle&#46;&#99;%6F&#46;u&#107;'>i&#110;&#102;o&#64;c&#97;rdi&#102;f&#117;nicyc&#108;&#101;&#46;co&#46;uk</a>

You can also use any of our social media services to get in touch with 
us and find out what's happening.
