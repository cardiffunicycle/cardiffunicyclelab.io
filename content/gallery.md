+++
title = "gallery"
weight = 20
+++

## Videos

Here is a video from one of our recent practice sessions.
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcardiffunicyclehockey%2Fvideos%2F1175868765842902%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

You can find more videos of matches and practice sessions on our [Facebook page](https://www.facebook.com/cardiffunicyclehockey/)

We will be adding videos to our [Youtube channel](https://www.youtube.com/channel/UCWE6nxMxGscF8tPiOrgelOQ) in the near future, so check back soon.


## Photos

![Barry and Ashley](/images/barry_and_ashley.jpg "A tussle")

Check on our [Facebook page](https://www.facebook.com/cardiffunicyclehockey/) for more.

We will be adding more photographs soon.

