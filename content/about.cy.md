+++
title = "amdanom"
weight = 10
menuname = "amdanom"
+++
## Beth yw Hoci Beic Un Olwyn?

Mae hoci beic un olwyn yn chwaraeon tîm sy'n hwyl iawn ac yn gwbl gynhwysol. Mae'n gymysgedd o rhyweddau ac nid oes cyfyngiadau o ran oed. Yr unig beth sy'n ofynnol i unrhyw un yw rhywfaint o sgiliau mewn beicio ar un olwyn.

Mae'r rheolau wedi'u diffinio gan [International Unicycling Federation](https://unicycling.org) ac mae'r set diweddaraf a gyhoeddwyd ar gael [yma](https://unicycling.org/files/iuf-rulebook-reorg.pdf#part.14).

Chwaraeir hoci beic un olwyn pump ychydig ar bob ochr ar gort dan do, fel arfer tua 35-45m o hyd ac 20-25m o led. Mae'r gôr wedi'i osod yn ôl o'r waliau pen-achu.
Mae pob chwaraewr yn reidio beiciau un olwyn ac yn cario ffyn hoci iâ. Rydyn ni'n defnyddio pêl tennis.

![cort hoci beic un olwyn](/images/unicycle_hockey_court.png "Cort hoci beic un olwyn")

## Pwy yw Tîm Hoci Beic Un Olwyn Caerdydd?

Rydym wedi bod yn cyfarfod fel tîm i ymarfer am tua 8 mlynedd. Rydym wedi cynnal twrnameintiau i gystadlu yn erbyn timau eraill yn y DU ac rydym hefyd wedi teithio'n rhyngwladol i gystadlu yng nghynghrair Pencampwriaeth Ewrop.

![ffotograff tîm](/images/team_photo.jpg "Rhai o aelodau'r clwb")

Rydym yn cyfarfod bob nos Sul am 8pm yn [Cardiff City House of Sport](http://www.cardiffcityhouseofsport.co.uk) i ddysgu pobl i beicio un olwyn ac i hyfforddi am 90 munud.

## Beth nesaf?

- Edrychwch ar ein [digwyddiadau sydd ar y gweill](#digwyddiadau).
- Edrychwch ar ein [oriel o luniau a fideos](#oriel).
- Dewch draw a [ymunwch â'r clwb](#ymuno).
- Cysylltwch â ni drwy [danfon neges](#cyswllt).
- Dilynwch ni ar gyfryngau cymdeithasol
