+++
title = "about"
weight = 10
+++
## What is Unicycle Hockey?

Unicycle hockey is a team sport that is great fun and totally inclusive. It is mixed gender and there are no age limits. The only thing that anyone requires is some skill in riding a unicycle.
The rules are defined by the [International Unicycling Fedaration](https://unicycling.org) and the latest published set is [here](https://unicycling.org/files/iuf-rulebook-reorg.pdf#part.14).

It is played five-a-side on an indoor court, usually around 35-45m in length and 20-25m in width. The court is surrounded by barriers and the goals are set back from the end walls.
All players ride unicycles and carry ice hockey sticks. We use a tennis ball

![unicycle hockey court](/images/unicycle_hockey_court.png "Unicycle hockey court")

## Who are the Cardiff Unicycle Hockey Team?

We have been meeting as a team to practice for around 8 years. We have 
held tournaments to compete against other teams in the UK and we 
have also travelled internationally to compete in 
European Championship tournaments.

![team photo](/images/team_photo.jpg "Some of out club members")

We meet every Sunday evening at 8pm at [Cardiff City House of Sport](https://www.cardiffcityhouseofsport.co.uk) to teach people to unicycle and to train for 90 minutes.

## What next?

* Check out our [upcoming events](#events).
* Have a lok at our [gallery of photos and videos](#gallery).
* Come along and [join the club](#join).
* Get in touch by [sending us a message](#contact)
* Follow us on social media
