+++
title = "events"
weight = 40
+++

## Weekly Practice

We meet every Sunday evening at 8:00 pm to practice unicycle hockey.

The address where this practice takes place is:

    Hall 3
    Cardiff City House of Sport
    Clos Park Morganwg
    Cardiff
    CF11 8AW

The first 45 minutes is a beginners' session where people can practice
unicycling and join in the game if they feel ready.

We use the second 45 minute session as an opportunity for players 
who are already proficient to enhance their skills.

Feel free to use [Meetup.com](https://www.meetup.com/cardiffunicycle/) 
or any of our other social media services if you would like to register 
your interest. Alernatively, [send us a message](#contact) or just turn 
up at a practice session.

## UK Tournaments

We intend to hold our next tournament in Cardiff in early June 2017.

Check back soon for more information.

## International Tournaments

We will be taking a team to the [European Championships](http://ecunicycling2017.eu/) 
which sart on July 30th in Sittard-Geleen in the Netherlands.

Check back soon for more information on our progress.

