+++
title = "join"
weight = 30
+++

Just coming along to practice with us is enough to make you a club member.

We have a simple pricing structure for each practice session:

* Our *learn to unicycle* sessions are free for anyone until such time 
that they can ride at least 50 metres unaided.
* Children, students, OAPs and other concessions pay at most £3 
* Players visiting from other clubs pay £3
* Other participants pay no more than £5

Since we have a limited numer of unicycles that we can offer to people 
for our training sessions, we advise people to [get in touch](#contact) 
with us to make sure that we can make sure that they have a unicycle to use.

